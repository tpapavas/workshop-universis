import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {BsModalRef, CollapseModule, ModalModule, TooltipModule} from 'ngx-bootstrap';
import {GradesService} from '../../services/grades.service';
import {GradeScaleService} from '@universis/common';
import {TranslateModule} from '@ngx-translate/core';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {ConfigurationService} from '@universis/common';
import {GradesThesesComponent} from './grades-theses.component';
import {TestingConfigurationService} from '../../../test';
import {MostModule} from '@themost/angular';
import {SharedModule} from '@universis/common';
import {RouterTestingModule} from '@angular/router/testing';
import {NgPipesModule} from 'ngx-pipes';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {LoadingService} from '@universis/common';
import { ProfileService } from 'src/app/profile/services/profile.service';

describe('GradesThesesComponent', () => {
    let component: GradesThesesComponent;
    let fixture: ComponentFixture<GradesThesesComponent>;
    const loadingSvc = jasmine.createSpyObj('LoadingService', ['showLoading', 'hideLoading']);
    beforeEach(async(() => {
        return TestBed.configureTestingModule({
            declarations: [GradesThesesComponent],
            providers: [
              BsModalRef,
              GradesService,
              GradeScaleService,
              ProfileService,
              {
                  provide: ConfigurationService,
                  useClass: TestingConfigurationService
              },
              {
                provide: LoadingService,
                useValue: loadingSvc
              }
            ],
            imports: [
              HttpClientTestingModule,
              RouterTestingModule,
              TranslateModule.forRoot(),
              MostModule.forRoot({
                  base: '/',
                  options: {
                      useMediaTypeExtensions: false
                  }
              }),
              ModalModule.forRoot(),
              SharedModule
            ],
          schemas: [
            CUSTOM_ELEMENTS_SCHEMA
          ]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(GradesThesesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
