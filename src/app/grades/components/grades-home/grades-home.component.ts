import { Component, OnInit } from '@angular/core';
import {GradesService} from '../../services/grades.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-grades-home',
  templateUrl: './grades-home.component.html',
  styleUrls: ['./grades-home.component.scss']
})
export class GradesHomeComponent implements OnInit {
  viewMode = 'tab1';

  constructor(private gradeService: GradesService, private _router: Router) {
  }

  ngOnInit() {
    if (this._router.url.toString() === '/grades/recent') {
      this.viewMode = 'tab1';
    } else  if (this._router.url.toString() === '/grades/all') {
      this.viewMode = 'tab2';
    } else  if (this._router.url.toString() === '/grades/theses') {
      this.viewMode = 'tab3';
    }
  }

}
