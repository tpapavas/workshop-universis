import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RequestsRoutingModule} from './requests-routing.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import {environment} from '../../environments/environment';
import {RequestsListComponent} from './components/requests-list/requests-list.component';
import {RequestsHomeComponent} from './components/requests-home/requests-home.component';
import {RequestsNewComponent} from './components/requests-new/requests-new.component';
import {ModalModule} from 'ngx-bootstrap';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import {RequestsSharedModule} from './requests-shared.module';
import { ElementsModule } from '../elements/elements.module';
import { NgPipesModule } from 'ngx-pipes';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TranslateModule,
        SharedModule,
        InfiniteScrollModule,
        RequestsRoutingModule,
        RequestsSharedModule,
        ModalModule,
        ElementsModule,
        NgPipesModule
    ],
    declarations: [RequestsListComponent, RequestsHomeComponent, RequestsNewComponent ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RequestsModule {
    constructor() {}
}
