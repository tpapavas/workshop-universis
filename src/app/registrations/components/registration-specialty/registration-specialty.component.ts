import { Component, OnInit } from '@angular/core';
import { CurrentRegistrationService } from '../../services/currentRegistrationService.service';
import { Router } from '@angular/router';
import {BsModalRef} from 'ngx-bootstrap/modal';
import { ProfileService } from 'src/app/profile/services/profile.service';

@Component({
  selector: 'app-registration-specialty',
  templateUrl: './registration-specialty.component.html',
  styleUrls: ['./registration-specialty.component.scss']
})
export class RegistrationSpecialtyComponent implements OnInit {

  public specialties;
  public selectedSpecialty;
  public currentSpecialty;
  public isLoading = true;
  public disabled = false;
  public lastError;
  public validationResults;
  constructor(public bsModalRef: BsModalRef,
              private currentReg: CurrentRegistrationService,
              private _profileService: ProfileService,
              private _router: Router) { }

  ngOnInit() {
    this.currentReg.getSpecialties().then(res => {
      this.specialties = res.specialties;
      this.currentReg.getCurrentSpecialty().then(currentSpec => {
        this.currentSpecialty = currentSpec;
        this.isLoading = false;
      });
    });
  }

  selectSpecialty() {
    this.lastError = null;
    const studyProgramSpecialty = this.specialties.find( x => {
      return x.id === this.selectedSpecialty;
    });
    this.disabled = true;
    this.currentReg.setSpecialty(studyProgramSpecialty).then(() => {
      sessionStorage.removeItem('student');
      sessionStorage.removeItem('registrationEffectiveStatus');
      sessionStorage.removeItem('availableClasses');
      this.bsModalRef.hide();
      this.disabled = false;     
      window.location.reload(true);
    }).catch( err => {
      // log error to console
      this.disabled = false;
      console.log(err);
      this.lastError = err;
      // customization for error 422
      if (err.error.statusCode =='422'){
        this.lastError.status = '422.1';
      }     
      if (err.error.validationResults){
        this.validationResults = err.error.validationResults;
      }
    });
  }
}
