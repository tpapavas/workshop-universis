#!/usr/bin/env node

/*eslint-env node*/
/*eslint no-var: "off"*/
var fs = require('fs');
var rfs = require('rotating-file-stream');
var path = require('path');
var express = require('express');
var http = require('http');
var morgan = require('morgan');

// initialize express application
// https://expressjs.com/en/api.html#express
var app = express();

// setup logger directory
var logDir = path.join(process.cwd(), 'log');
// ensure log directory exists
fs.existsSync(logDir) || fs.mkdirSync(logDir);
// create a rotating write stream
var accessLogStream = rfs('access.log', {
    interval: '1d', // rotate daily
    path: logDir
});

// setup the logger
// https://github.com/expressjs/morgan
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" ":x-forwarded-for"', { stream: accessLogStream }));
// setup morgan token remote-addr
morgan.token('x-forwarded-for', function (req) {
    return req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
});

// static files
// https://expressjs.com/en/starter/static-files.html
// disable caching while serving app.production.json
app.use('/assets/config/app.production.json', express.static(path.join(process.cwd(), 'dist/assets/config/app.production.json'), {
    lastModified: false,
    cacheControl: false,
    etag: false
}));
// serve all other static files
app.use(express.static(path.join(process.cwd(), 'dist')));

// get port from environment and store in Express.
var port = normalizePort(process.env.PORT || '7001');
app.set('port', port);

// get ip from environment and store in Express.
var address = process.env.IP || '127.0.0.1';
app.set('address', address);

// create http server
var server = http.createServer(app);

// Listen on provided port and address
server.listen(port, address, function() {
    if (process.send) {
        process.send('online');
    } else {
        //
    }
});
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val) {
    var port = parseInt(val, 10);
    if (isNaN(port)) {
        // named pipe
        return val;
    }
    if (port >= 0) {
        // port number
        return port;
    }
    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    var bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;
    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
    try {
        const addr = server.address();
        console.log('Listening on http://' + addr.address + ':' + addr.port)
    }
    catch(err) {
        // do nothing
    }
}