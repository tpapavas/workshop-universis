# Contributing to universis-student

1. Fork the project
2. Code and submit a Merge Request
  - Make sure you follow the style guidelines (found at the end of this documents)
3. We will review the Merge Request, leave comments, and merge it when ready

## Coding

### Installation

  Check out the [installation guide](https://gitlab.com/universis/universis-students/blob/master/INSTALL.md)

### See it on your browser

  - Run `ng serve` for a devevelopment server
  - Navigate to `http://localhost:7001/`

  The app will automatically reload if you change any of the source files.

### Scaffolding

- Run `ng generate component component-name` to generate a new component.

- You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.


* Run `ng build` to build the project.

The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Tests

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Style

1. Only English words allowed within the code

2. Make sure tests run successfully prior to opening a new Merge Request (MR)

3. We follow the best practices for git, for example:
  - Commit message starts with a verb in the first person (eg. Add, Fix)
  - There is an empty line at the end of each file
  - There are no spaces at the end of a line

## Bugs

Found a bug? Open an [issue](https://gitlab.com/universis/universis-student/issues/new)
